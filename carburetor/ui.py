#!/usr/bin/python3
# Released under GPLv3+ License
# Danial Behzadi<dani.behzi@ubuntu.com>, 2020

import gi
import os
gi.require_versions({'Gtk': '3.0'})
from gi.repository import Gdk, GLib, Gtk
from . import config
from . import handler

s_data_dir = config.s_data_dir
builder = Gtk.Builder()

def initialize_builder():
    builder.add_from_file(s_data_dir+'/about.ui')
    builder.add_from_file(s_data_dir+'/main.ui')
    builder.add_from_file(s_data_dir+'/preferences.ui')
    builder.connect_signals(handler.Handler())

def get(obj):
    return builder.get_object(obj)

def css():
    css_provider = Gtk.CssProvider()
    css_provider.load_from_path(s_data_dir+'/style.css')
    screen = Gdk.Screen.get_default()
    context = Gtk.StyleContext()
    context.add_provider_for_screen(screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_USER)
