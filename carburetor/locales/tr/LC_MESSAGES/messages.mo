��    :      �  O   �      �     �          "     3     K     ]     p  8   �  :   �  @     J   B  3   �  K   �  :        H     P     \     d     m     v     �  
   �     �     �     �     �     �     �     �     �     �     �  "   �                     (     /  	   4     >  
   S  	   ^     h     n     t     y     �     �     �     �     �     �     �     �     �     	      	  �  ,	     �
     
     )  '   E      m     �  0   �  G   �  @   !  E   b  L   �  7   �  U   -  G   �  	   �     �  
   �     �                &  
   -     8  
   ?     J     Q     W     _     h     p     y     �  3   �     �     �     �     �     �     �       
             (     1     9     @  	   H     R     j     �     �     �     �     �     �     �          !   8       :   7   $                        /      .               ,              )                         6                           5   0       
   -          %             +          &      "   4      *   (   2   1          #         3   9           '   	                        <b>Accept connection</b> <b>DNS Port</b> <b>Exit node</b> <b>HTTP Tunnel Port</b> <b>Socks Port</b> <b>Use bridges</b> <b>obfs4proxy executable</b> <small>Bridges help you to bypass tor sensorship</small> <small>Local port on which Tractor would be listen</small> <small>Local port on which a HTTP tunnel would be listen</small> <small>Local port on which you would have an anonymous name server</small> <small>The country you want to connect from</small> <small>Whether or not allowing external devices to use this network</small> <small>You should specify where is obfs4proxy file</small> Austria Auto (Best) Bridges Bridges: Bulgaria Can not save the bridges Canada Carburetor Czech Finland France General Germany Ireland Moldova Netherlands New ID Norway Please check the syntax of bridges Poland Ports Romania Russia Save Set Proxy Settings for Tractor Seychelles Singapore Spain Start Stop Sweden Switzerland Tractor is not running! Tractor is running Tractor is stopped Ukraine United Kingdom United States Unset Proxy You have a new identity! starting… stopping… Project-Id-Version: Turkish (Carburetor)
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-10-06 23:26+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Turkish <https://hosted.weblate.org/projects/carburetor/translations/tr/>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.3-dev
 <b>Bağlantı kabul et</b> <b>DNS Bağlantı Noktası</b> <b>Çıkış düğümü</b> <b>HTTP Tüneli Bağlantı Noktası</b> <b>Socks Bağlantı Noktası</b> <b>Köprüleri kullan</b> <b>obfs4proxy çalıştırılabilir dosyası</b> <small>Köprüler, tor sansürünü atlamanıza yardımcı olur</small> <small>Tractor'un dinleyeceği yerel bağlantı noktası</small> <small>HTTP tünelinin dinleyeceği yerel bağlantı noktası</small> <small>Anonim bir ad sunucunuzun olacağı yerel bağlantı noktası</small> <small>Üzerinden bağlanmak istediğiniz ülke</small> <small>Harici aygıtların bu ağı kullanmasına izin verilip verilmeyeceği</small> <small>obfs4proxy dosyasının nerede olduğunu belirtmelisiniz</small> Avusturya Otomatik (En iyisi) Köprüler Köprüler: Bulgaristan Köprüler kaydedilemiyor Kanada Carburetor Çekya Finlandiya Fransa Genel Almanya İrlanda Moldova Hollanda Yeni kimlik Norveç Lütfen köprülerin söz dizimini gözden geçirin Polonya Bağlantı Noktaları Romanya Rusya Kaydet Vekili Ayarla Tractor Ayarları Seyşeller Singapur İspanya Başlat Durdur İsveç İsviçre Tractor çalışmıyor! Tractor çalışıyor Tractor durduruldu Ukrayna Birleşik Krallık Amerika Birleşik Devletleri Vekili Kaldır Yeni bir kimliğiniz var! başlatılıyor… durduruluyor… 