#!/usr/bin/python3
# Released under GPLv3+ License
# Danial Behzadi<dani.behzi@ubuntu.com>, 2019-2020

import gi
gi.require_versions({'Gdk': '3.0', 'Gtk': '3.0'})
from gi.repository import Gdk, Gio, Gtk
from sys import argv
from . import ui

class AppWindow(Gtk.ApplicationWindow):#TODO: Use Handy.ApplicationWindow
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        ui.initialize_builder()
        self.set_icon_name("carburetor")
        self.load_css()
        self.connect("key-press-event", self.on_key_press)

        header_bar = ui.get('HeaderBar')
        self.set_titlebar(header_bar)

        main_container = ui.get('MainContainer')
        self.add(main_container)

    def load_css(self):
        ui.css()

    def on_key_press(self, widget, event):#TODO: check why it's not working anymore
        keyname = Gdk.keyval_name(event.keyval)
        if keyname == 'F10':
            menu_button = ui.get('MenuButton')
            menu_button.activate()
        return False


class Application(Gtk.Application):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, application_id='org.tractor.carburetor', **kwargs)
        self.window = None
        self.prefs = None

    def do_startup(self):
        Gtk.Application.do_startup(self)

        action = Gio.SimpleAction.new('preferences', None)
        action.connect('activate', self.on_preferences)
        self.add_action(action)

        action = Gio.SimpleAction.new('about', None)
        action.connect('activate', self.on_about)
        self.add_action(action)

        action = Gio.SimpleAction.new("quit", None)
        action.connect('activate', self.on_quit)
        self.add_action(action)

    def do_activate(self):
        if not self.window:
            window = AppWindow(application=self)
            self.window = window

        self.window.present()

    def on_preferences(self, action, param):
        PreferencesWindow = ui.get('PreferencesWindow')
        PreferencesWindow.present()

    def on_about(self, action, param):
        about_dialog = ui.get('AboutDialog')
        about_dialog.present()

    def on_quit(self, action, param):
        self.quit()

def main():
    app = Application()
    app.run(argv)

if __name__ == '__main__':
    main()
