#!/usr/bin/python3
# Released under GPLv3+ License
# Danial Behzadi<dani.behzi@ubuntu.com>, 2020

import gi
import os
import re
from distutils.util import strtobool
gi.require_versions({'Notify': '0.7'})
from gi.repository import Gio, GLib, Gtk, Notify
from subprocess import PIPE, Popen
from . import config
from . import ui

command = "tractor "
dconf = config.dconf
_ = config._
app_name = config.app_name
version = config.version


class Handler:
    def is_running(self):
        check = Popen(command + 'isrunning', stdout=PIPE, shell=True)
        output = strtobool(check.stdout.read().decode('utf-8').strip('\n'))
        return output

    def is_proxy_set(self):
        proxy = Gio.Settings.new('org.gnome.system.proxy')
        socks = Gio.Settings.new('org.gnome.system.proxy.socks')
        if dconf.get_boolean('accept-connection'):
            ip = '0.0.0.0'
        else:
            ip = '127.0.0.1'
        proxy_mode = proxy.get_string('mode')
        current_port = socks.get_int('port')
        tractor_port = dconf.get_int('socks-port')
        current_ip = socks.get_string('host')
        if proxy_mode=='manual' and current_port==tractor_port and current_ip==ip:
            return True
        else:
            return False

    def set_to_stopped(self):
        image = ui.get('MainImage')
        image.set_from_icon_name('process-stop', Gtk.IconSize.DIALOG)
        label = ui.get('MainLabel')
        label.set_label(_("Stopped"))
        button = ui.get('StartButton')
        text_start = _("Start")
        button.set_tooltip_text(text_start)
        icon = Gtk.Image()
        icon.set_from_icon_name('media-playback-start', Gtk.IconSize.MENU)
        button.props.image = icon
        self.notification = _("Tractor is stopped")

    def set_to_running(self):
        image = ui.get('MainImage')
        image.set_from_icon_name('security-high', Gtk.IconSize.DIALOG)
        label = ui.get('MainLabel')
        label.set_label(_("Running"))
        button = ui.get('StartButton')
        text_stop = _("Stop")
        button.set_tooltip_text(text_stop)
        icon = Gtk.Image()
        icon.set_from_icon_name('process-stop', Gtk.IconSize.MENU)
        button.props.image = icon
        self.notification = _("Tractor is running")

    def set_run_status(self):
        if self.is_running():
            self.set_to_running()
        else:
            self.set_to_stopped()

    def notify(self):
        notif = Notify.Notification.new(app_name, self.notification)
        notif.set_timeout(Notify.EXPIRES_DEFAULT)
        notif.show()

    def on_MainBox_realize(self, box):
        self.set_run_status()

    def on_StartButton_clicked(self, button):
        button.set_sensitive(False)
        button.spinner = Gtk.Spinner()
        button.props.image = button.spinner
        button.spinner.start()
        if self.is_running():
            text_stopping = _("stopping…")
            button.set_tooltip_text(text_stopping)
            action = 'stop'
        else:
            text_starting = _("starting…")
            button.set_tooltip_text(text_starting)
            action = 'start'
        self.task = Popen(command + action, stdout=PIPE, shell=True, preexec_fn=os.setsid)
        self.io_id = GLib.io_add_watch(self.task.stdout, GLib.IO_IN, self.set_progress)
        GLib.io_add_watch(self.task.stdout, GLib.IO_HUP, self.thread_finished, button)

    def set_progress (self, stdout, condition):
        try:
            line = stdout.readline().decode('utf-8')
            label = ui.get('MainLabel')
            valid = re.compile(r'.*\[notice\] ')
            if 'notice' in line:
                notice = valid.sub('', line)[:-5]
                label.set_label(notice)
        except ValueError:
            pass
        return True

    def thread_finished (self, stdout, condition, button):
        GLib.source_remove(self.io_id)
        stdout.close()
        button.spinner.stop()
        self.set_run_status()
        button.set_sensitive(True)
        self.notify()
        return False

    def on_SetButton_realize(self, button):
        self.SetButton_set_label(button)

    def on_SetButton_toggled(self, button):
        if self.is_proxy_set():
            action = 'unset'
        else:
            action = 'set'
        task = Popen(command + action, stdout=PIPE, shell=True)
        task.wait()
        self.SetButton_set_label(button)

    def SetButton_set_label(self, button):
        #TODO now the button will always start as not active and toggles regardless of state, by uncommenting the set-active lines, if the proxy was set and you run the app, it toggles from inactive to active and toggles the button in a loop! I should think for a solution, but I should harry up and go out now. So I'll postpone it :D
        if self.is_proxy_set():
            text_set_button = _("Unset Proxy")
            #button.set_active(True)
        else:
            text_set_button = _("Set Proxy")
            #button.set_active(False)
        button.set_tooltip_text(text_set_button)

    def on_NewButton_clicked(self, button):
        if self.is_running():
            newid = Popen(command + 'newid', stdout=PIPE, shell=True)
            newid.wait()
            notif = Notify.Notification.new(app_name, _("You have a new identity!"))
        else:
            notif = Notify.Notification.new(app_name, _("Tractor is not running!"))
        notif.set_timeout(Notify.EXPIRES_DEFAULT)
        notif.show()

    def on_ActionExitNode_realize(self, combo):
        nodes = {_("Austria"): "au",
                _("Bulgaria"): "bg",
                _("Canada"): "ca",
                _("Czech"): "cz",
                _("Finland"): "fi",
                _("France"): "fr",
                _("Germany"): "de",
                _("Ireland"): "ie",
                _("Moldova"): "md",
                _("Netherlands"): "nl",
                _("Norway"): "no",
                _("Poland"): "pl",
                _("Romania"): "ro",
                _("Russia"): "su",
                _("Seychelles"): "sc",
                _("Singapore"): "sg",
                _("Spain"): "es",
                _("Sweden"): "se",
                _("Switzerland"): "ch",
                _("Ukraine"): "ua",
                _("United Kingdom"): "uk",
                _("United States"): "us"}
        combo.append("ww", _("Auto (Best)"))
        for node in sorted(nodes.keys()):
            combo.append(nodes[node], node)
        dconf.bind("exit-node", combo, "active-id", Gio.SettingsBindFlags.DEFAULT)

    def on_ActionAcceptConnection_realize(self, switch):
        dconf.bind("accept-connection", switch, "active", Gio.SettingsBindFlags.DEFAULT)

    def on_ActionSocksPort_realize(self, spin):
        port = dconf.get_int("socks-port")
        adjustment = Gtk.Adjustment(value=port, lower=1, upper=65535, step_increment=1, page_increment=1, page_size=0)
        spin.set_adjustment(adjustment)
        spin.set_text(str(port))
        dconf.bind("socks-port", spin, "value", Gio.SettingsBindFlags.DEFAULT)

    def on_ActionDNSPort_realize(self, spin):
        port = dconf.get_int("dns-port")
        adjustment = Gtk.Adjustment(value=port, lower=1, upper=65535, step_increment=1, page_increment=1, page_size=0)
        spin.set_adjustment(adjustment)
        spin.set_text(str(port))
        dconf.bind("dns-port", spin, "value", Gio.SettingsBindFlags.DEFAULT)

    def on_ActionHTTPPort_realize(self, spin):
        port = dconf.get_int("http-port")
        adjustment = Gtk.Adjustment(value=port, lower=1, upper=65535, step_increment=1, page_increment=1, page_size=0)
        spin.set_adjustment(adjustment)
        spin.set_text(str(port))
        dconf.bind("http-port", spin, "value", Gio.SettingsBindFlags.DEFAULT)

    def on_ActionBridgeType_realize(self, switch):
        dconf.bind("use-bridges", switch, "active", Gio.SettingsBindFlags.DEFAULT)

    def on_TextO4Bridge_realize(self, view):
        buff = view.get_buffer()
        get_bridges = Popen(command + "bridgesfile", stdout=PIPE, shell=True)
        bridges = get_bridges.stdout.read()
        bridges_file = bridges.decode("utf-8").strip('\n')
        with open(bridges_file) as file:
            text = file.read()
            buff.set_text(str(text))

    def on_ButtonO4Bridge_clicked(self, button):
        textview = ui.get('TextO4Bridge')
        buff = textview.get_buffer()
        text = buff.get_text(buff.get_start_iter(), buff.get_end_iter(), 0)
        regex = re.compile(r'^( )*([Bb][Rr][Ii][Dd][Gg][Ee])?( )*', re.MULTILINE)
        get_bridges = Popen(command + "bridgesfile", stdout=PIPE, shell=True)
        bridges = get_bridges.stdout.read()
        bridges_file = bridges.decode("utf-8").strip('\n')
        if text == regex.sub('Bridge ', text):
            with open(bridges_file, 'w') as file:
                file.write(text)
        else:
            dialog = ui.get('O4ErrorDialog')
            response = dialog.run()
            if -6 < response < -3:
                dialog.close()

    def on_ActionBridgePlugin_realize(self, chooser):
        def on_file_set(chooser):
            filename = chooser.get_filename()
            dconf.set_string("obfs4-path", filename)
        current_file = Gio.File.new_for_path(dconf.get_string("obfs4-path"))
        chooser.set_file(current_file)
        chooser.connect("file-set", on_file_set)

    def on_AboutDialog_realize(self, dialog):
        dialog.set_program_name(app_name)
        dialog.set_version(version)
        dialog.set_translator_credits(_("translator-credits"))


