#!/usr/bin/python3
# Released under GPLv3+ License
# Danial Behzadi<dani.behzi@ubuntu.com>, 2020

import re
import setuptools


with open("README.md", "r") as readme:
    long_description = readme.read()

with open("debian/changelog", "r") as changelog:
    latest = changelog.readline()
version = re.split('\(|\)', latest)[1]

with open("debian/control", "r") as control:
    for line in control:
        if line.startswith("Maintainer: "):
            maintainer = re.split(': | <|>', line)
        elif line.startswith("Description: "):
            description = re.split(': |\n', line)[1]
        elif line.startswith("Package: "):
            name = line.split()[1]


setuptools.setup(
    name=name,
    version=version,
    author=maintainer[1],
    author_email=maintainer[2],
    description=description,
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://framagit.org/tractor/carburetor",
    packages=setuptools.find_packages(),
    package_data={
        'carburetor': ['about.ui', 'main.ui',
            'preferences.ui', 'style.css', 'locales']
    },
    include_package_data = True,
    project_urls={
        "Bug Tra cker": "https://framagit.org/tractor/carburetor/-/issues",
        "Documentation": "https://framagit.org/tractor/carburetor/-/blob/master/man/tractor.1",
        "Source Code": "https://framagit.org/tractor/carburetor",
    },
    install_requires=[
        'traxtor',
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    entry_points={
        "gui_scripts": [
            "carburetor = carburetor.carburetor:main",
        ],
    }
)
